export const ALBUM_API = "https://rallycoding.herokuapp.com/api/music_albums";
export interface IAlbum {
  title: string;
  artist: string;
  url: string;
  image: string;
  thumbnail_image: string;
}
