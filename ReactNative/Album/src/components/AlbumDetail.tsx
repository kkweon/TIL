import * as React from "react";

import { Linking, Image, View, Text, StyleSheet } from "react-native";
import { IAlbum } from "../config";
import Button from "../layouts/Button";
import Card from "../layouts/Card";
import CardSection from "../layouts/CardSection";

export default ({ album }: { album: IAlbum }) => {
  return (
    <Card>
      <CardSection>
        <View style={style.thumbnailContainer}>
          <Image
            style={style.thumbnail}
            resizeMode="cover"
            source={{ uri: album.thumbnail_image }}
          />
        </View>
        <View style={style.description}>
          <Text style={style.title}>{album.title}</Text>
          <Text style={style.artist}>{album.artist}</Text>
        </View>
      </CardSection>

      <CardSection>
        <Image style={style.albumImage} source={{ uri: album.image }} />
      </CardSection>

      <Button onPress={() => Linking.openURL(album.url)} text="Buy It" />
    </Card>
  );
};

const style = StyleSheet.create({
  description: {
    flexDirection: "column",
    justifyContent: "space-around",
  },

  title: {
    fontSize: 20,
  },

  artist: {
    fontStyle: "italic",
  },

  thumbnail: {
    height: 50,
    width: 50,
  },

  albumImage: {
    height: 300,
    flex: 1,
  },

  thumbnailContainer: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
    marginRight: 10,
  },
});
