import * as React from "react";
import { ScrollView } from "react-native";
import axios, { AxiosResponse } from "axios";
import { ALBUM_API, IAlbum } from "../config";
import AlbumDetail from "./AlbumDetail";

interface IOwnState {
  albums: IAlbum[];
}

export default class extends React.Component<{}, IOwnState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      albums: [],
    };
  }

  componentWillMount() {
    axios.get(ALBUM_API).then((res: AxiosResponse<IAlbum[]>) => {
      console.log("============");
      console.log(res.data);
      console.log("============");
      this.setState({ albums: res.data });
    });
  }

  renderAlbum(album: IAlbum) {
    return <AlbumDetail key={album.url} album={album} />;
  }

  render() {
    const albums = this.state.albums.map(this.renderAlbum);
    return <ScrollView>{albums}</ScrollView>;
  }
}
