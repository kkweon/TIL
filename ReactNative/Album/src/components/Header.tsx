import * as React from "react";
import { Platform, Text, View, StyleSheet } from "react-native";

const style = StyleSheet.create({
  viewStyle: {
    backgroundColor: "#f8f8f8",
    justifyContent: "center",
    alignItems: "center",
    height: 60,
    paddingTop: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    ...Platform.select({
      ios: {
        shadowOpacity: 0.2,
      },
      android: {
        elevation: 5,
      },
    }),
    position: "relative",
  },
  textStyle: {
    fontSize: 20,
  },
});

interface IProp {
  text: string;
}

export default (props: IProp) => {
  return (
    <View style={style.viewStyle}>
      <Text style={style.textStyle}>{props.text}</Text>
    </View>
  );
};
