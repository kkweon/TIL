import * as React from "react";
import { Text, TouchableOpacity, StyleSheet } from "react-native";

interface IProps {
  text: string;
  onPress: () => void;
}

export default (props: IProps) => {
  return (
    <TouchableOpacity onPress={props.onPress} style={style.buttonStyle}>
      <Text style={style.textStyle}>{props.text}</Text>
    </TouchableOpacity>
  );
};

const style = StyleSheet.create({
  textStyle: {
    alignSelf: "center",
    color: "#007aff",
    fontSize: 16,
    fontWeight: "600",
  },
  buttonStyle: {
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "#fff",
    borderColor: "#007aff",
    borderRadius: 5,
    borderWidth: 1,
    height: 40,
    justifyContent: "center",
  },
});
