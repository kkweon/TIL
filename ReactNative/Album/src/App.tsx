/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import * as React from "react";
import { View } from "react-native";

import Header from "./components/Header";
import AlbumList from "./components/AlbumList";

export default () => (
  <View style={{ flex: 1, paddingBottom: 10 }}>
    <Header text="Header" />
    <AlbumList />
  </View>
);
