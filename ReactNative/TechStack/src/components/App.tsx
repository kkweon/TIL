import * as React from "react";
import { View } from "react-native";
import { Provider } from "react-redux";
import { createStore } from "redux";

import reducers from "../reducers";
import { Header } from "./commons";
import LibraryList from "./LibraryList";

const store = createStore(reducers);

export default () => {
  return (
    <Provider store={store}>
      <View style={{ flex: 1 }}>
        <Header headerText="Tech Stack" />
        <LibraryList />
      </View>
    </Provider>
  );
};
