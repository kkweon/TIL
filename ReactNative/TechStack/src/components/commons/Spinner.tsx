import * as React from "react";
import { ActivityIndicator, StyleSheet, View } from "react-native";

interface IProps {
  size: number | "large" | "small";
}

export default ({ size }: IProps) => {
  return (
    <View style={styles.spinnerStyle}>
      <ActivityIndicator size={size || "large"} />
    </View>
  );
};

const styles = StyleSheet.create({
  spinnerStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
