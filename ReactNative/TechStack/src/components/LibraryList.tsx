import * as React from "react";
import { ListView, ListViewDataSource, View } from "react-native";
import { connect } from "react-redux";
import { IReduxState } from "../reducers";
import { ILibrary } from "../reducers/library-reducer";
import ListItem from "./ListItem";

interface IStateProps {
  libraries: ILibrary[];
}

class LibraryList extends React.Component<IStateProps> {
  private dataSource: ListViewDataSource | null = null;

  public componentWillMount() {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });

    this.dataSource = ds.cloneWithRows(this.props.libraries);
  }

  public render() {
    if (this.dataSource) {
      return (
        <ListView dataSource={this.dataSource} renderRow={this.renderRow} />
      );
    }

    return <View />;
  }

  private renderRow(rowData: ILibrary) {
    return <ListItem item={rowData} />;
  }
}

function mapStateProps(state: IReduxState): IStateProps {
  return { libraries: state.libraries };
}

export default connect(mapStateProps)(LibraryList);
