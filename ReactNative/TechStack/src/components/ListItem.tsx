import * as React from "react";
import {
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  NativeModules,
  LayoutAnimation,
  View,
} from "react-native";
import { connect } from "react-redux";

import { selectLibrary } from "../actions";
import { IReduxState } from "../reducers";
import { ILibrary } from "../reducers/library-reducer";
import { CardSection } from "./commons";

interface IOwnProps {
  item: ILibrary;
}

interface IStateProps {
  expanded: boolean;
}

interface IDispatchProps {
  selectLibrary: (id: number) => void;
}

type IProps = IOwnProps & IDispatchProps & IStateProps;

// register animation
const { UIManager } = NativeModules;
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

class ListItem extends React.Component<IProps> {
  componentWillUpdate() {
    LayoutAnimation.spring();
  }
  public render() {
    const { id, title } = this.props.item;
    return (
      <TouchableWithoutFeedback onPress={() => this.props.selectLibrary(id)}>
        <View>
          <CardSection>
            <Text style={styles.title}>{title}</Text>
          </CardSection>
          {this.renderDescription()}
        </View>
      </TouchableWithoutFeedback>
    );
  }

  private renderDescription() {
    if (this.props.expanded)
      return (
        <CardSection>
          <Text style={styles.description}>{this.props.item.description}</Text>
        </CardSection>
      );
    return <View />;
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 16,
    paddingLeft: 16,
  },

  description: {
    paddingLeft: 16,
  },
});

function mapStateToProps(state: IReduxState, ownProps: IProps): IStateProps {
  const expanded = state.selectedLibraryId === ownProps.item.id;
  return {
    expanded,
  };
}

export default connect<IStateProps, IDispatchProps, IProps, IReduxState>(
  mapStateToProps,
  {
    selectLibrary,
  },
)(ListItem);
