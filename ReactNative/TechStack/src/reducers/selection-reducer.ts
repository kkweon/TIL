import { Action, ActionKeys } from "../actions/types";

export type IChoice = number | null;

export default (state: IChoice = null, action: Action) => {
  switch (action.type) {
    case ActionKeys.SELECT_LIBRARY:
      return action.payload;

    default:
      return state;
  }
};
