import { Action } from "../actions/types";
import data from "./library.data.json";

export interface ILibrary {
  id: number;
  title: string;
  description: string;
}

export default (state: ILibrary[] = data, action: Action) => {
  switch (action.type) {
    default:
      return state;
  }
};
