import { combineReducers } from "redux";
import libraryReducer, { ILibrary } from "./library-reducer";
import SelectionReducer, { IChoice } from "./selection-reducer";

export interface IReduxState {
  libraries: ILibrary[];
  selectedLibraryId: IChoice;
}

export default combineReducers<IReduxState>({
  libraries: libraryReducer,
  selectedLibraryId: SelectionReducer,
} as any);
