export enum ActionKeys {
  SELECT_LIBRARY = "SELECT_LIBRARY",
}

export interface IActionSelect {
  readonly type: ActionKeys.SELECT_LIBRARY;
  readonly payload: number;
}

export type Action = IActionSelect;
