import { ActionKeys, IActionSelect } from "./types";

export const selectLibrary = (id: number): IActionSelect => {
  return {
    payload: id,
    type: ActionKeys.SELECT_LIBRARY,
  };
};
