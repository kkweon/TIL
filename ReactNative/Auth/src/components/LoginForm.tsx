import * as React from "react";
import {
  StyleSheet,
  Button,
  Text,
  View,
  Dimensions,
  ActivityIndicator,
} from "react-native";
import { Field } from "./commons";
import { FirebaseError } from "firebase/app";

interface IOwnProps {
  error: FirebaseError | null;
  loading: boolean;
  handleLogin: (email: string, password: string) => void;
}

interface IOwnState {
  email: string;
  password: string;
}

export default class LoginForm extends React.Component<IOwnProps, IOwnState> {
  state: IOwnState = {
    email: "",
    password: "",
  };

  onLoading() {
    if (this.props.loading) {
      return (
        <ActivityIndicator
          style={styles.spinner}
          animating={this.props.loading}
          size="large"
          color="#0000ff"
        />
      );
    }

    return (
      <Button
        title="Log In"
        onPress={() =>
          this.props.handleLogin(this.state.email, this.state.password)}
      />
    );
  }

  render() {
    return (
      <View
        style={this.props.loading ? styles.loadingStyle : styles.formContainer}
      >
        <Field
          onChangeText={(email: string) => this.setState({ email })}
          label="Email"
          placeholder="example@gmail.com"
        />
        <Field
          onChangeText={(password: string) => this.setState({ password })}
          label="Password"
          placeholder="password"
          secureTextEntry
        />
        <Text style={styles.error}>
          {this.props.error ? this.props.error.message : ""}
        </Text>
        {this.onLoading()}
      </View>
    );
  }
}

const { width: deviceWidth } = Dimensions.get("window");

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
  },

  formContainer: {
    flex: 1,
    width: deviceWidth * 0.8,
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
  },

  spinner: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
  },

  loadingStyle: {
    flex: 1,
    width: deviceWidth * 0.8,
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
    opacity: 0.5,
  },

  error: {
    color: "red",
    textAlign: "center",
    marginBottom: 5,
  },
});
