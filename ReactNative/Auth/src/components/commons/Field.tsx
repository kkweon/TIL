import * as React from "react";
import { Text, View, TextInput, StyleSheet } from "react-native";

interface IProps {
  label: string;
  placeholder: string;
  onChangeText?: (text: string) => void;
  secureTextEntry?: boolean;
}

export const Field = (props: IProps) => {
  return (
    <View style={styles.containerStyle}>
      <Text style={styles.label}>{props.label}</Text>
      <TextInput
        style={styles.input}
        placeholder={props.placeholder}
        onChangeText={props.onChangeText}
        secureTextEntry={props.secureTextEntry}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    flexDirection: "row",
    alignItems: "center",
  },

  label: {
    flex: 1,
  },
  input: {
    flex: 3,
  },
});
