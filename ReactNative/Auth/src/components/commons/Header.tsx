import * as React from "react";
import { Text, View, StyleSheet } from "react-native";

interface IProp {
  text: string;
}

export const Header = ({ text }: IProp) => {
  return (
    <View style={styles.containerStyle}>
      <Text>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    alignItems: "center",
    justifyContent: "center",
    height: 50,
    borderTopWidth: 2,
    borderBottomWidth: 2,
    borderColor: "#ddd",
    elevation: 1,
  },
});
