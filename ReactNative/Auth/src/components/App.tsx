/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import * as firebase from "firebase/app";
import * as React from "react";
import { View, StyleSheet } from "react-native";
import { Header } from "./commons";
import LoginForm from "./LoginForm";
import Feature from "./Feature";

interface IState {
  email: string;
  password: string;
  user: firebase.User | null;
  error: firebase.FirebaseError | null;
  loading: boolean;
}

export default class App extends React.Component<{}, IState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      email: "",
      password: "",
      user: null,
      error: null,
      loading: false,
    };
  }

  loginSuccessful(user: firebase.User) {
    this.setState({ user, error: null, loading: false });
  }

  createFail(error: firebase.FirebaseError) {
    this.setState({ error, loading: false });
  }

  handleLogin(email: string, password: string) {
    this.setState({ loading: true });
    console.log("[firebase] Trying to login");
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(this.loginSuccessful.bind(this))
      .catch((error: firebase.FirebaseError) => {
        // Fail to login
        this.createFail(error);

        // Try to create
        if (error.code === "auth/user-not-found")
          firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then(this.loginSuccessful.bind(this))
            .catch(this.createFail.bind(this));
      });
  }

  handleLogout() {
    firebase
      .auth()
      .signOut()
      .then(() => {
        this.setState({ user: null });
      });
  }

  render() {
    const content = this.state.user ? (
      <Feature handleLogout={this.handleLogout.bind(this)} />
    ) : (
      <LoginForm
        error={this.state.error}
        loading={this.state.loading}
        handleLogin={this.handleLogin.bind(this)}
      />
    );

    return (
      <View style={styles.containerStyle}>
        <Header text="Auth" />
        {content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
  },
});
