import * as React from "react";
import { View, Button, Text, StyleSheet } from "react-native";

interface IOwnProps {
  handleLogout: () => void;
}

export default (props: IOwnProps) => {
  return (
    <View style={styles.container}>
      <Text style={styles.text}>Logged In</Text>
      <Button title="Log Out" onPress={() => props.handleLogout()} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },

  text: {
    justifyContent: "center",
    alignItems: "center",
  },
});
