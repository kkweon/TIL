import * as firebase from "firebase/app";
import { FirebaseError } from "firebase/app";
import { Actions } from "react-native-router-flux";
import { Dispatch } from "react-redux";
import {
  Action,
  ActionKeys,
  IEmailChangeAction,
  IPasswordChangeAction,
} from "./types";

export const emailChanged = (email: string): IEmailChangeAction => {
  return {
    payload: email,
    type: ActionKeys.EMAIL_CHANGE,
  };
};

export const passwordChanged = (password: string): IPasswordChangeAction => {
  return {
    payload: password,
    type: ActionKeys.PASSWORD_CHANGE,
  };
};

interface IUserCredential {
  email: string;
  password: string;
}

export const loginUser = ({ email, password }: IUserCredential) => {
  return (dispatch: Dispatch<Action>) => {
    // loading true
    loginLoading(dispatch);

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((user: firebase.User) => loginUserSuccess(dispatch, user))
      .catch((error: firebase.FirebaseError) => loginUserFail(dispatch, error));
  };
};

const loginLoading = (dispatch: Dispatch<Action>) => {
  dispatch({
    type: ActionKeys.AUTH_LOADING,
  });
};
const loginUserSuccess = (dispatch: Dispatch<Action>, user: firebase.User) => {
  dispatch({
    payload: user,
    type: ActionKeys.AUTH_SUCCESS,
  });

  // <Scene key="main" />
  Actions.main();
};
const loginUserFail = (dispatch: Dispatch<Action>, error: FirebaseError) => {
  dispatch({
    payload: error,
    type: ActionKeys.AUTH_FAIL,
  });
};
