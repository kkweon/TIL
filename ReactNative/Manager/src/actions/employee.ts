import * as firebase from "firebase/app";
import { Actions } from "react-native-router-flux";
import { Dispatch } from "react-redux";

export enum EmployeeActionKeys {
  EMPLOYEE_UPDATE = "EMPLOYEE_UPDATE",
  EMPLOYEE_CREATE = "EMPLOYEE_CREATE",
  EMPLOYEES_FETCH_SUCCESS = "EMPLOYEES_FETCH_SUCCESS",
  EMPLOYEE_FORM_CLEAR = "EMPLOYEE_FORM_CLEAR",
}

// update
export interface IEmployeeUpdateAction<T> {
  readonly type: EmployeeActionKeys.EMPLOYEE_UPDATE;
  readonly payload: IEmployeeUpdatePayload<T>;
}

export interface IEmployeeUpdatePayload<T> {
  prop: "name" | "phone" | "shift";
  value: T;
}

export const employeeUpdate = <T>({
  prop,
  value,
}: IEmployeeUpdatePayload<T>): IEmployeeUpdateAction<T> => {
  return {
    payload: { prop, value },
    type: EmployeeActionKeys.EMPLOYEE_UPDATE,
  };
};

// create
export interface IEmployeeCreateAction {
  readonly type: EmployeeActionKeys.EMPLOYEE_CREATE;
}
export const employeeCreate = ({ name, phone, shift }: IEmployeeForm) => {
  const { currentUser } = firebase.auth();

  return (dispatch: Dispatch<EmployeeAction>) => {
    firebase
      .database()
      .ref(`/users/${currentUser && currentUser.uid}/employees`)
      .push({ name, phone, shift })
      .then(() => {
        dispatch({ type: EmployeeActionKeys.EMPLOYEE_CREATE });
        Actions.pop();
      });
  };
};

// fetch
export interface IEmployeeFetchSuccessAction {
  readonly type: EmployeeActionKeys.EMPLOYEES_FETCH_SUCCESS;
  readonly payload: IEmployeeRawFirebase;
}
export const employeesFetch = () => {
  const { currentUser } = firebase.auth();
  return (dispatch: Dispatch<EmployeeAction>) => {
    firebase
      .database()
      .ref(`/users/${currentUser && currentUser.uid}/employees`)
      .on("value", (snapshot: firebase.database.DataSnapshot | null) => {
        if (snapshot) {
          dispatch({
            payload: snapshot.val(),
            type: EmployeeActionKeys.EMPLOYEES_FETCH_SUCCESS,
          });
        }
      });
  };
};

// clear form
export interface IEmployeeClearFormAction {
  readonly type: EmployeeActionKeys.EMPLOYEE_FORM_CLEAR;
}
export const clearEmployeeForm = (): IEmployeeClearFormAction => {
  return {
    type: EmployeeActionKeys.EMPLOYEE_FORM_CLEAR,
  };
};

// save
export const employeeSave = ({ name, phone, shift, uid }: IEmployeeWithUID) => {
  const { currentUser } = firebase.auth();
  return () => {
    firebase
      .database()
      .ref(`/users/${currentUser && currentUser.uid}/employees/${uid}`)
      .set({ name, phone, shift })
      .then(() => {
        Actions.pop();
      });
  };
};

// delete
export const employeeDelete = (uid: string) => {
  const { currentUser } = firebase.auth();
  return () => {
    firebase
      .database()
      .ref(`/users/${currentUser && currentUser.uid}/employees/${uid}`)
      .remove()
      .then(() => {
        Actions.pop();
      });
  };
};

export type EmployeeAction =
  | IEmployeeUpdateAction<any>
  | IEmployeeCreateAction
  | IEmployeeFetchSuccessAction
  | IEmployeeClearFormAction;
