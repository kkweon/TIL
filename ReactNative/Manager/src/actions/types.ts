import * as firebase from "firebase/app";
import { EmployeeAction } from "./employee";

export enum ActionKeys {
  EMAIL_CHANGE = "EMAIL_CHANGE",
  PASSWORD_CHANGE = "PASSWORD_CHANGE",
  AUTH_LOADING = "AUTH_LOADING",
  AUTH_SUCCESS = "AUTH_USER",
  AUTH_FAIL = "AUTH_FAIL",
}

export interface IEmailChangeAction {
  readonly type: ActionKeys.EMAIL_CHANGE;
  readonly payload: string;
}

export interface IPasswordChangeAction {
  readonly type: ActionKeys.PASSWORD_CHANGE;
  readonly payload: string;
}

export interface IAuthLoadingAction {
  readonly type: ActionKeys.AUTH_LOADING;
}

export interface IAuthSuccessAction {
  readonly type: ActionKeys.AUTH_SUCCESS;
  readonly payload: firebase.User;
}

export interface IAuthFailAction {
  readonly type: ActionKeys.AUTH_FAIL;
  readonly payload: firebase.FirebaseError;
}

export type Action =
  | IEmailChangeAction
  | IPasswordChangeAction
  | IAuthLoadingAction
  | IAuthSuccessAction
  | IAuthFailAction
  | EmployeeAction;
