interface IEmployeeForm {
  name: string;
  phone: string;
  shift: string;
}

interface IEmployeeWithUID extends IEmployeeForm {
  uid: string;
}

interface IEmployeeRawFirebase {
  [key: string]: IEmployeeForm;
}
