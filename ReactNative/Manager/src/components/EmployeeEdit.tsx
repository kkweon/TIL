import _ from "lodash";
import * as React from "react";
import Communications from "react-native-communications";
import { connect } from "react-redux";
import {
  employeeDelete,
  employeeSave,
  employeeUpdate,
  IEmployeeUpdatePayload,
} from "../actions/employee";
import { IRootState } from "../reducers";
import { Button, Card, CardSection, Confirm } from "./commons";
import EmployeeForm from "./EmployeeForm";

interface IOwnProps {
  employee: IEmployeeWithUID;
}

type IStateProps = IEmployeeForm;

interface IDispatchProps {
  employeeUpdate: ({ prop, value }: IEmployeeUpdatePayload<string>) => void;
  employeeSave: (employee: IEmployeeWithUID) => void;
  employeeDelete: (uid: string) => void;
}

interface IState {
  showModal: boolean;
}

type Props = IOwnProps & IStateProps & IDispatchProps;
class EmployeeEdit extends React.Component<Props, IState> {
  constructor(props: Props) {
    super(props);
    this.state = { showModal: false };
  }
  public componentWillMount() {
    _.each(this.props.employee, (value: string, prop: string) => {
      if (prop === "name" || prop === "phone" || prop === "shift") {
        this.props.employeeUpdate({ prop, value });
      }
    });
  }

  public render() {
    return (
      <Card>
        <EmployeeForm />

        <CardSection>
          <Button onPress={this.onButtonPress.bind(this)}>Save Changes</Button>
        </CardSection>

        <CardSection>
          <Button onPress={this.onTextPress.bind(this)}>Text Schedule</Button>
        </CardSection>

        <CardSection>
          <Button
            onPress={() => this.setState({ showModal: !this.state.showModal })}
          >
            Fire Employee
          </Button>
        </CardSection>

        <Confirm
          onAccept={this.onAccept.bind(this)}
          onDecline={this.onDecline.bind(this)}
          visible={this.state.showModal}
        >
          Are you sure you want to delete this?
        </Confirm>
      </Card>
    );
  }

  private onAccept() {
    const { uid } = this.props.employee;
    this.props.employeeDelete(uid);
    this.setState({ showModal: false });
  }
  private onDecline() {
    this.setState({ showModal: false });
  }

  private onTextPress() {
    const { phone, shift } = this.props;
    Communications.text(phone, `Your upcoming shift is on ${shift}`);
  }

  private onButtonPress() {
    const { name, phone, shift } = this.props;
    const { uid } = this.props.employee;
    this.props.employeeSave({ name, phone, shift, uid });
  }
}

function mapStateToProps(state: IRootState): IStateProps {
  const { name, phone, shift } = state.employeeForm;
  return {
    name,
    phone,
    shift,
  };
}

export default connect<IStateProps, IDispatchProps, {}, IRootState>(
  mapStateToProps,
  {
    employeeUpdate,
    employeeSave,
    employeeDelete,
  },
)(EmployeeEdit);
