/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import * as React from "react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { applyMiddleware } from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import reducers from "../reducers";
import Router from "../router";

const store = createStore(reducers, applyMiddleware(thunk, logger));

export default class App extends React.Component {
  public render() {
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}
