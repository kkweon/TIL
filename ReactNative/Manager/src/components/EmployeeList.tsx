import _ from "lodash";
import * as React from "react";
import { ListView, ListViewDataSource } from "react-native";
import { connect } from "react-redux";
import { employeesFetch } from "../actions/employee";
import { IRootState } from "../reducers";
import ListItem from "./EmployeeListItem";

interface IStateProps {
  employees: IEmployeeWithUID[];
}

interface IDispatchProps {
  employeesFetch: () => void;
}

type Props = IStateProps & IDispatchProps;

class EmployeeList extends React.Component<Props> {
  protected dataSource: ListViewDataSource | null;

  constructor(props: Props) {
    super(props);
    this.dataSource = null;
  }

  public componentWillMount() {
    this.props.employeesFetch();
    this.createDataSource(this.props);
  }

  public componentWillReceiveProps(nextProps: Props) {
    this.createDataSource(nextProps);
  }

  public render() {
    if (this.dataSource) {
      return (
        <ListView
          enableEmptySections
          dataSource={this.dataSource}
          renderRow={this.renderRow}
        />
      );
    }
    return;
  }

  private renderRow(employee: IEmployeeWithUID) {
    return <ListItem employee={employee} />;
  }

  private createDataSource({ employees }: Props) {
    const ds = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    });
    this.dataSource = ds.cloneWithRows(employees);
  }
}

function mapStateToProps(state: IRootState): IStateProps {
  const employees = _.map(state.employees, (val, uid) => {
    return { ...val, uid };
  });

  return {
    employees,
  };
}

export default connect<IStateProps, IDispatchProps, {}, IRootState>(
  mapStateToProps,
  {
    employeesFetch,
  },
)(EmployeeList);
