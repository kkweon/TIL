import * as React from "react";
import { Picker, StyleSheet, Text, View } from "react-native";
import { connect } from "react-redux";
import { employeeUpdate, IEmployeeUpdatePayload } from "../actions/employee";
import { IRootState } from "../reducers";
import { CardSection, Input } from "./commons";

type IStateProps = IEmployeeForm;

interface IDispatchProps<T> {
  employeeUpdate: (payload: IEmployeeUpdatePayload<T>) => void;
}

type Props = IDispatchProps<string> & IStateProps;

class EmployeeForm extends React.Component<Props> {
  public render() {
    return (
      <View>
        <CardSection>
          <Input
            onChangeText={(value: string) =>
              this.props.employeeUpdate({ prop: "name", value })
            }
            value={this.props.name}
            label="Name"
            placeholder="Jane"
          />
        </CardSection>

        <CardSection>
          <Input
            value={this.props.phone}
            label="Phone"
            placeholder="555-555-5555"
            onChangeText={(value: string) =>
              this.props.employeeUpdate({ prop: "phone", value })
            }
          />
        </CardSection>

        <CardSection style={{ flexDirection: "column" }}>
          <Text style={styles.pickerTextStyle}>Shift</Text>
          <Picker
            selectedValue={this.props.shift}
            onValueChange={(value: string) =>
              this.props.employeeUpdate({ prop: "shift", value })
            }
          >
            <Picker.Item label="Monday" value="Monday" />
            <Picker.Item label="Tuesday" value="Tuesday" />
            <Picker.Item label="Wednesday" value="Wednesday" />
            <Picker.Item label="Thursday" value="Thursday" />
            <Picker.Item label="Friday" value="Friday" />
            <Picker.Item label="Saturday" value="Saturday" />
            <Picker.Item label="Sunday" value="Sunday" />
          </Picker>
        </CardSection>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  pickerTextStyle: {
    fontSize: 18,
    paddingLeft: 20,
  },
});

function mapStateToProps(state: IRootState): IStateProps {
  const { name, phone, shift } = state.employeeForm;
  return {
    name,
    phone,
    shift,
  };
}

export default connect<IStateProps, IDispatchProps<string>, {}, IRootState>(
  mapStateToProps,
  {
    employeeUpdate,
  },
)(EmployeeForm);
