import { FirebaseError } from "firebase/app";
import * as React from "react";
import { Text } from "react-native";
import { connect } from "react-redux";
import { emailChanged, loginUser, passwordChanged } from "../actions/auth";
import { IRootState } from "../reducers";
import { Button, Card, CardSection, Input, Spinner } from "./commons";

type Props = IDispatchProps & IStateProps;

class LoginForm extends React.Component<Props> {
  /**
   * When email type changes
   */
  public onEmailChange(text: string) {
    this.props.emailChanged(text);
  }

  /**
   * When password changes
   */
  public onPasswordChange(text: string) {
    this.props.passwordChanged(text);
  }

  /**
   * When login button is clicked
   */
  public onButtonPress() {
    const { email, password } = this.props;
    this.props.loginUser({ email, password });
  }

  public renderButtonOrSpinner() {
    if (this.props.loading) {
      return <Spinner />;
    }
    return <Button onPress={this.onButtonPress.bind(this)}>Login</Button>;
  }

  public render() {
    return (
      <Card>
        <CardSection>
          <Input
            onChangeText={this.onEmailChange.bind(this)}
            label="Email"
            placeholder="email@gmail.com"
            value={this.props.email}
          />
        </CardSection>

        <CardSection>
          <Input
            onChangeText={this.onPasswordChange.bind(this)}
            secureTextEntry
            label="Password"
            placeholder="password"
            value={this.props.password}
          />
        </CardSection>

        <CardSection>{this.renderButtonOrSpinner()}</CardSection>

        <CardSection>
          <Text style={{ color: "red" }}>
            {this.props.error && this.props.error.message}
          </Text>
        </CardSection>
      </Card>
    );
  }
}

interface IStateProps {
  email: string;
  password: string;
  error: FirebaseError | null;
  loading: boolean;
}

interface IDispatchProps {
  emailChanged: (text: string) => void;
  passwordChanged: (text: string) => void;
  loginUser: ({ email, password }: { email: string; password: string }) => void;
}

const mapStateToProps = ({
  auth: { email, password, error, loading },
}: IRootState): IStateProps => {
  return {
    email,
    password,
    error,
    loading,
  };
};

export default connect<IStateProps, IDispatchProps, {}, IRootState>(
  mapStateToProps,
  {
    emailChanged,
    passwordChanged,
    loginUser,
  },
)(LoginForm);
