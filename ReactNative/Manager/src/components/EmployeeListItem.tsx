import * as React from "react";
import { StyleSheet, Text, TouchableWithoutFeedback, View } from "react-native";
import { Actions } from "react-native-router-flux";
import { CardSection } from "./commons";

interface IOwnProps {
  employee: IEmployeeWithUID;
}

export default class ListItem extends React.Component<IOwnProps> {
  public onRowPress() {
    Actions.employeeEdit({ employee: this.props.employee });
  }
  public render() {
    const { name } = this.props.employee;

    return (
      <TouchableWithoutFeedback onPress={this.onRowPress.bind(this)}>
        <View>
          <CardSection>
            <Text style={styles.titleStyle}>{name}</Text>
          </CardSection>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 18,
    paddingLeft: 15,
  },
});
