import * as React from "react";
import {
  ImageStyle,
  StyleProp,
  StyleSheet,
  TextStyle,
  View,
  ViewStyle,
} from "react-native";

interface IProps {
  children: any;
  style?: StyleProp<ViewStyle | TextStyle | ImageStyle>;
}
export default (props: IProps) => {
  return (
    <View style={[styles.containerStyle, props.style]}>{props.children}</View>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderColor: "#ddd",
    flexDirection: "row",
    justifyContent: "flex-start",
    padding: 5,
    position: "relative",
  },
});
