import * as React from "react";
import { connect } from "react-redux";
import { clearEmployeeForm, employeeCreate } from "../actions/employee";
import { IRootState } from "../reducers";
import { Button, Card, CardSection } from "./commons";
import EmployeeForm from "./EmployeeForm";

interface IStateProps {
  name: string;
  phone: string;
  shift: string;
}

interface IDispatchProps {
  employeeCreate: (form: IEmployeeForm) => void;
  clearEmployeeForm: () => void;
}

interface IOwnProps {
  employee: IEmployeeWithUID;
}

type Props = IStateProps & IDispatchProps & IOwnProps;
class EmployeeCreate extends React.Component<Props> {
  public componentWillMount() {
    this.props.clearEmployeeForm();
  }
  public render() {
    return (
      <Card>
        <EmployeeForm {...this.props} />
        <CardSection>
          <Button onPress={this.onButtonPress.bind(this)}>Create</Button>
        </CardSection>
      </Card>
    );
  }

  private onButtonPress() {
    const { name, phone, shift } = this.props;
    this.props.employeeCreate({ name, phone, shift: shift || "Monday" });
  }
}

const mapStateToProps = (state: IRootState): IStateProps => {
  const { name, phone, shift } = state.employeeForm;
  return {
    name,
    phone,
    shift,
  };
};
export default connect<IStateProps, IDispatchProps, {}, IRootState>(
  mapStateToProps,
  {
    employeeCreate,
    clearEmployeeForm,
  },
)(EmployeeCreate);
