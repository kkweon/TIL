import * as React from "react";
import { Actions, Router, Scene, Stack } from "react-native-router-flux";
import EmployeeCreate from "./components/EmployeeCreate";
import EmployeeEdit from "./components/EmployeeEdit";
import EmployeeList from "./components/EmployeeList";
import LoginForm from "./components/LoginForm";

export default () => {
  return (
    <Router>
      <Stack key="root" hideNavBar>
        <Stack key="auth">
          <Scene
            key="login"
            component={LoginForm}
            title="Please Login"
            initial
          />
        </Stack>

        <Stack key="main">
          <Scene
            rightTitle="Add"
            rightButtonStyle={{ right: 0 }}
            onRight={() => {
              Actions.employeeCreate();
            }}
            key="employeeList"
            component={EmployeeList}
            title="Employees"
          />
          <Scene
            key="employeeCreate"
            title="Create Employee"
            component={EmployeeCreate}
          />

          <Scene
            key="employeeEdit"
            title="Edit Employee"
            component={EmployeeEdit}
          />
        </Stack>
      </Stack>
    </Router>
  );
};
