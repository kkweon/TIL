import { combineReducers } from "redux";
import { authReducer, IAuthState } from "./auth";
import employeeReducer from "./employee";
import employeeFormReducer from "./employee-form";

export interface IRootState {
  auth: IAuthState;
  employeeForm: IEmployeeForm;
  employees: IEmployeeRawFirebase;
}

export default combineReducers<IRootState>({
  auth: authReducer,
  employeeForm: employeeFormReducer,
  employees: employeeReducer,
} as any);
