import { EmployeeActionKeys } from "../actions/employee";
import { Action } from "../actions/types";

const INITIAL_STATE: IEmployeeForm = {
  name: "",
  phone: "",
  shift: "",
};

export default (
  state: IEmployeeForm = INITIAL_STATE,
  action: Action,
): IEmployeeForm => {
  switch (action.type) {
    case EmployeeActionKeys.EMPLOYEE_UPDATE:
      const { prop, value } = action.payload;
      return {
        ...state,
        [prop]: value,
      };

    case EmployeeActionKeys.EMPLOYEE_FORM_CLEAR:
    case EmployeeActionKeys.EMPLOYEE_CREATE:
      return INITIAL_STATE;

    default:
      return state;
  }
};
