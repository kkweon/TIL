import { FirebaseError, User } from "firebase/app";
import { Action, ActionKeys } from "../actions/types";

export interface IAuthState {
  email: string;
  password: string;
  error: FirebaseError | null;
  loading: boolean;
  user: User | null;
}

const initState: IAuthState = {
  email: "",
  error: null,
  loading: false,
  password: "",
  user: null,
};

export const authReducer = (
  state: IAuthState = initState,
  action: Action,
): IAuthState => {
  switch (action.type) {
    case ActionKeys.EMAIL_CHANGE:
      return {
        ...state,
        email: action.payload,
      };

    case ActionKeys.PASSWORD_CHANGE:
      return {
        ...state,
        password: action.payload,
      };

    case ActionKeys.AUTH_LOADING:
      return {
        ...state,
        error: null,
        loading: true,
      };

    case ActionKeys.AUTH_FAIL:
      return {
        ...state,
        error: action.payload,
        loading: false,
      };

    case ActionKeys.AUTH_SUCCESS:
      return {
        ...state,
        ...initState,
        user: action.payload,
      };

    default:
      return state;
  }
};
