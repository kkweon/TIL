import { EmployeeActionKeys } from "../actions/employee";
import { Action } from "../actions/types";

const INITIAL_STATE = {};
export default (
  state: IEmployeeRawFirebase = INITIAL_STATE,
  action: Action,
): IEmployeeRawFirebase => {
  switch (action.type) {
    case EmployeeActionKeys.EMPLOYEES_FETCH_SUCCESS:
      return action.payload;

    default:
      return state;
  }
};
