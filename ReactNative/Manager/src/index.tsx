import { AppRegistry } from "react-native";
import App from "./components/App";

import * as firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

const firebaseConfig = {
  apiKey: "AIzaSyDuljQ_b7rXOnr0ts5uZBSa9uDCidYOttc",
  authDomain: "authreactnative-f6474.firebaseapp.com",
  databaseURL: "https://authreactnative-f6474.firebaseio.com",
  messagingSenderId: "872872907197",
  projectId: "authreactnative-f6474",
  storageBucket: "authreactnative-f6474.appspot.com",
};
firebase.initializeApp(firebaseConfig);

AppRegistry.registerComponent("Manager", () => App);
