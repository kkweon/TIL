// @flow
import { renderComponent, expect } from "../helper";
import Header from "../../src/components/header";
import { describe, beforeEach, it } from "mocha";

describe("Header", () => {
  let component;

  beforeEach(() => {
    component = renderComponent(Header);
  });

  it("renders something", () => {
    expect(component).to.exist;
  });

  it("contains 3 links", () => {
    expect(component.find("a")).to.have.lengthOf(3);
  });
});
