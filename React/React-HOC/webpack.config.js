module.exports = {
  entry: "./src/index.jsx",

  output: {
    path: __dirname,
    publicPath: "/",
    filename: "bundle.js",
  },

  devtool: "inline-source-map",

  module: {
    rules: [
      { test: /\.jsx?$/, exclude: /node_modules/, loader: "babel-loader" },
    ],
  },
  resolve: {
    extensions: [".jsx", ".js"],
  },
  devServer: {
    historyApiFallback: true,
    contentBase: "./",
    port: 4000,
    open: true,
  },
};
