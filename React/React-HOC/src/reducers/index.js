// @flow
import { combineReducers } from "redux";
import authReducer from "./authentication";

const rootReducer = combineReducers({
  authenticated: authReducer,
});

export type ReduxState = {
  authenticated: boolean,
};

export default rootReducer;
