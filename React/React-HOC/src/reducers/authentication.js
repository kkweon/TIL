// @flow
import * as types from "../actions/types";
import type { AuthAction } from "../actions";

export default function(state: boolean = false, action: AuthAction): boolean {
  switch (action.type) {
    case types.CHANGE_AUTH:
      return action.payload;
    default:
      return state;
  }
}
