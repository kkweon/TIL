// @flow
import * as types from "./types";

export type AuthAction = {
  type: string,
  payload: boolean,
};

export function authenticate(isLoggedIn: boolean): AuthAction {
  return {
    type: types.CHANGE_AUTH,
    payload: isLoggedIn,
  };
}
