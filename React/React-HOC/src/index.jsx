// @flow
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import logger from "redux-logger";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import requireAuth from "./components/require_auth";
import App from "./components/app";
import Resource from "./components/resource";
import reducers from "./reducers";
import Header from "./components/header";

const store = createStore(
  reducers,
  // applyMiddleware() tells createStore() how to handle middleware
  applyMiddleware(logger),
);
const mountNode = document.querySelector(".container");
if (!mountNode) throw new Error("mountNode is not found");

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <div>
        <Header />

        {/* Not visible */}
        <Switch>
          <Route exact path="/" component={App} />
          <Route path="/resources" component={requireAuth(Resource)} />
        </Switch>
      </div>
    </Router>
  </Provider>,
  mountNode,
);
