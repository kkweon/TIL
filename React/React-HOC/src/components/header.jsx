// @flow
import * as React from "react";
import { connect } from "react-redux";
import * as actions from "../actions";
import type { ReduxState } from "../reducers";
import { Link } from "react-router-dom";

type Props = {
  authenticated: boolean,
  authenticate: (loggedin: boolean) => any,
};
type State = {};

class Header extends React.Component<Props, State> {
  authButton() {
    const authFn = () => this.props.authenticate(!this.props.authenticated);
    if (this.props.authenticated)
      return <button onClick={authFn}>Sign out</button>;
    return <button onClick={authFn}>Sign In</button>;
  }
  render() {
    return (
      <div>
        <nav className="navbar navbar-light">
          <ul className="nav navbar-nav">
            <li className="nav-item">
              <Link to="/">Home</Link>
            </li>

            <li className="nav-item">
              <Link to="/resources">Resources</Link>
            </li>

            <li className="nav-item">
              <Link to="/login">{this.authButton()}</Link>
            </li>
          </ul>
        </nav>
      </div>
    );
  }
}

function mapStateToProps(state: ReduxState): any {
  return {
    authenticated: state.authenticated,
  };
}

export default connect(mapStateToProps, actions)(Header);
