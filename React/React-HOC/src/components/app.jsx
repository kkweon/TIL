// @flow
import React, { Component } from "react";
import { connect } from "react-redux";
import Header from "./header";

type Props = {
  children: any,
};
type State = {};

class App extends Component<Props, State> {
  render() {
    return (
      <div>
        <h2>App</h2>
        <h3>this.props.children</h3>
        {this.props.children}
      </div>
    );
  }
}

export default connect()(App);
