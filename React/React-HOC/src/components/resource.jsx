// @flow
import React, { Component } from "react";

export default () => {
  return (
    <div>
      Super Special Secret Recipe
      <ul>
        <li>1 Cup Sugar</li>
        <li>1 Cup Paper</li>
        <li>1 Cup Salt</li>
      </ul>
    </div>
  );
};
