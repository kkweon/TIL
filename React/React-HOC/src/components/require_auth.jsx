// @flow
import * as React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import type { ReduxState } from "../reducers";

export default function(ComposedComponent: React.ComponentType<any>) {
  class Authentication extends React.Component<any, any> {
    static contextTypes = {
      router: PropTypes.object,
    };

    componentWillMount() {
      if (!this.props.authenticated) this.context.router.history.push("/");
    }

    componentWillUpdate(nextProps: any) {
      if (!nextProps.authenticated) this.context.router.history.push("/");
    }

    render() {
      console.log(this.context);
      console.log(this.props.authenticated);
      return (
        <div>
          <ComposedComponent {...this.props} />;
        </div>
      );
    }
  }

  function mapStateToProps(state: ReduxState): { authenticated: boolean } {
    return {
      authenticated: state.authenticated,
    };
  }

  return connect(mapStateToProps)(Authentication);
}
