import * as mongoose from "mongoose";

mongoose
  .connect(process.env.DB_URI || "")
  .then(_ => console.log("[Mogoose] Connected"))
  .catch(err => console.log("[Mongoose] Fail to connect", err));

export * from "./user";
