import * as bcrypt from "bcrypt-nodejs";
import * as mongoose from "mongoose";

export interface IUser extends mongoose.Document {
  email: string;
  password: string;
  comparePassword: (candidatePassword: string) => Promise<boolean>;
}

const userSchema = new mongoose.Schema({
  email: {
    lowercase: true,
    required: true,
    type: String,
    unique: true,
  },
  password: { type: String, required: true },
});

// before saving, run this function
userSchema.pre("save", function(this: IUser, next) {
  const user = this;
  bcrypt.genSalt(10, (err: Error, salt: string): void => {
    if (err) {
      return next(err);
    }

    bcrypt.hash(
      user.password,
      salt,
      () => null,
      (_: Error, hash: string) => {
        // overwrite password
        user.password = hash;
        next();
      },
    );
  });
});

userSchema.methods.comparePassword = function(
  candidatePassword: string,
): Promise<boolean> {
  const user = this;

  return new Promise((resolve: (isMatch: boolean) => void, reject) => {
    bcrypt.compare(
      candidatePassword,
      user.password,
      (err, isMatch: boolean) => {
        if (err) return reject(err);
        resolve(isMatch);
      },
    );
  });
};

export const User = mongoose.model<IUser>("User", userSchema);
