import * as bodyParser from "body-parser";
import * as cors from "cors";
import * as express from "express";
import * as morgan from "morgan";
import * as authController from "./controllers/authentication";

class Server {
  private port: number;
  private app: express.Application;

  constructor() {
    this.port = parseInt(process.env.PORT || "4000", 10);
    this.app = express();
    this.config();
    this.routes();
  }

  public run(): void {
    this.app.listen(this.port, () => {
      console.log(`[index] Server running at http://localhost:${this.port}`);
    });
  }

  private config(): void {
    this.app.use(morgan("combined"));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(cors());
  }

  private routes(): void {
    const rootRouter = express.Router();
    rootRouter.route("/").get((_, res) => {
      res.send({ message: "Hello Root" });
    });
    this.app.use("/", rootRouter);
    this.app.use("/auth", authController.userRouter);
  }
}

new Server().run();
