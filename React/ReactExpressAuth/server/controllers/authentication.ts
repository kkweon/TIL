import * as express from "express";
import * as HttpStatus from "http-status-codes";
import * as jwt from "jwt-simple";
import * as passport from "passport";

import { SECRET } from "../config";
import { IUser, User } from "../models";
import "../services/passport";

const requireAuth = passport.authenticate("jwt", { session: false });
const requireSignin = passport.authenticate("local", { session: false });

export const userRouter = express.Router();
userRouter.get("/", requireAuth, (_, res) => {
  res.send({ message: "Hi there" });
});
userRouter.post("/signup", signup);
userRouter.post("/signin", requireSignin, signin);

export interface IToken {
  sub: string;
  iat: number;
}

function tokenForUser(user: IUser): string {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, SECRET);
}

function signin(req: express.Request, res: express.Response): any {
  // User is already verified by passport, give them a token

  const token = tokenForUser(req.user);
  res.send({ token });
}

function signup(
  req: express.Request,
  res: express.Response,
  __: express.NextFunction,
): any {
  User.create(req.body)
    .then((newUser: IUser) => {
      const token = tokenForUser(newUser);
      res.send({ token });
    })
    .catch((reason: any) => res.status(HttpStatus.UNPROCESSABLE_ENTITY).send(reason));
}
