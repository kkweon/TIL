import * as passport from "passport";
import * as LocalSrategy from "passport-local";
import {
  ExtractJwt,
  Strategy as JwtStrategy,
  StrategyOptions,
  VerifiedCallback,
} from "passport-jwt";

import * as config from "../config";
import { IToken } from "../controllers/authentication";
import { IUser, User } from "../models";

// 1. setup options for jwt strategy & local strategy
const jwtOptions: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey: config.SECRET,
};

const localOptions = { usernameField: "email" };
const localLogin = new LocalSrategy.Strategy(
  localOptions,
  (email: string, password: string, done: any): void => {
    User.findOne({ email: email })
      .then(user => {
        if (!user) return done(null, false);

        user
          .comparePassword(password)
          .then(result => {
            if (result) return done(null, user);

            return done(null, result);
          })
          .catch(err => {
            done(err);
          });
      })
      .catch(err => {
        done(err);
      });
  },
);

// 2. create jwt strategy
const jwtLogin = new JwtStrategy(
  jwtOptions,
  (payload: IToken, done: VerifiedCallback) => {
    User.findById(payload.sub)
      .then((user: IUser | null) => {
        if (user) done(null, user);
        else done(null, false);
      })
      .catch(_ => {
        done(null, false);
      });
  },
);

// 3. tell passport
passport.use(jwtLogin);
passport.use(localLogin);
