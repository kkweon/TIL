const webpack = require("webpack");

module.exports = {
  entry: "./src/index.tsx",

  output: {
    path: __dirname,
    publicPath: "/",
    filename: "bundle.js",
  },

  devtool: "source-map",

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: "awesome-typescript-loader",
      },

      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  resolve: {
    extensions: [".jsx", ".js", ".tsx", ".ts"],
  },
  devServer: {
    historyApiFallback: true,
    contentBase: "./",
    port: parseInt(process.env.PORT || "5000", 10),
    host: "0.0.0.0",
    open: false,
  },

  plugins: [
    new webpack.ProvidePlugin({
      // inject ES5 modules as global vars
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      Tether: "tether",
    }),
  ],
};
