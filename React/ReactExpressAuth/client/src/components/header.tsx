import * as React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { IReduxState } from "../reducers";

interface IStateProps {
  authenticated: boolean;
}
interface IState {}

type IProps = IStateProps;

class Header extends React.Component<IProps, IState> {
  private renderAuth() {
    if (this.props.authenticated)
      return (
        <li className="nav-item">
          <NavLink className="nav-link" to="/signout" activeClassName="active">
            Sign Out
          </NavLink>
        </li>
      );
    else
      return (
        <React.Fragment>
          <li className="nav-item">
            <NavLink className="nav-link" to="/signin" activeClassName="active">
              Sign In
            </NavLink>
          </li>

          <li className="nav-item">
            <NavLink className="nav-link" to="/signup" activeClassName="active">
              Sign Up
            </NavLink>
          </li>
        </React.Fragment>
      );
  }
  public render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
          <NavLink className="navbar-brand" to="/" activeClassName="active">
            Home
          </NavLink>
          <button
            className="navbar-toggler navbar-toggler-right"
            type="button"
            data-toggle="collapse"
            data-target="#navbarToggler"
            aria-controls="navbarToggler"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarToggler">
            <ul className="navbar-nav">{this.renderAuth()}</ul>
          </div>
        </div>
      </nav>
    );
  }
}

function mapStateProps(state: IReduxState): IStateProps {
  return {
    authenticated: state.auth.authenticated,
  };
}

export default connect<IStateProps, {}, {}, IReduxState>(mapStateProps)(Header);
