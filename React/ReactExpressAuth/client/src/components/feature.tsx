import * as React from "react";
import { connect, Dispatch } from "react-redux";
import { fetchMessage } from "../actions";
import { IReduxState } from "../reducers";
import { IReceiveData } from "../actions/types";

interface IStateProps {
  message: string;
}
interface IDispatchProps {
  fetchMessage: () => (dispatch: Dispatch<IReceiveData>) => void;
}

class Feature extends React.Component<IDispatchProps & IStateProps> {
  componentWillMount() {
    this.props.fetchMessage();
  }

  render() {
    return (
      <div>
        <h1 className="display-4">Feature</h1>
        <p className="lead">{this.props.message}</p>
      </div>
    );
  }
}

function mapStateToProps(state: IReduxState): IStateProps {
  return {
    message: state.message,
  };
}

export default connect<
  IStateProps,
  IDispatchProps,
  {},
  IReduxState
>(mapStateToProps, { fetchMessage })(Feature);
