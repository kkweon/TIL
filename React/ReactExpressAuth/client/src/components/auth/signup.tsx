import * as React from "react";
import { connect, Dispatch } from "react-redux";
import {
  Field,
  reduxForm,
  InjectedFormProps,
  WrappedFieldMetaProps,
  ActionTypes,
  FormErrors,
  WrappedFieldInputProps,
} from "redux-form";
import { signupUser, IUserInfo } from "../../actions";
import { IReduxState } from "../../reducers";

interface IFormProps {
  email: string;
  password: string;
  password2: string;
}

interface IStateProps {
  errorMessage: string | null;
}

interface IDispatchProps {
  signupUser: (userInfo: IUserInfo) => void;
}

type Props = IStateProps & IDispatchProps & InjectedFormProps<IFormProps>;

class Signup extends React.Component<Props, {}> {
  private handleSubmit({ email, password }: IFormProps) {
    const userInfo: IUserInfo = {
      email,
      password,
    };
    this.props.signupUser(userInfo);
  }
  private renderErrorMessage() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger" role="alert">
          {this.props.errorMessage}
        </div>
      );
    }
    return;
  }
  public render() {
    return (
      <form onSubmit={this.props.handleSubmit(this.handleSubmit.bind(this))}>
        {this.renderErrorMessage()}

        <fieldset className="form-group">
          <Field
            component={renderField}
            className="form-control"
            name="email"
            label="Email"
            type="email"
          />
        </fieldset>

        <fieldset className="form-group">
          <Field
            component={renderField}
            className="form-control"
            name="password"
            label="Password"
            type="password"
          />
        </fieldset>

        <fieldset className="form-group">
          <Field
            component={renderField}
            className="form-control"
            name="password2"
            label="Confirm Password"
            type="password"
          />
        </fieldset>

        <button
          type="submit"
          disabled={this.props.invalid}
          className="btn btn-primary"
        >
          Sign Up
        </button>
      </form>
    );
  }
}

const validate = (values: IFormProps): FormErrors<IFormProps> => {
  const errors: FormErrors<IFormProps> = { email: "", password: "", password2: "" };

  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }

  if (!values.password) {
    errors.password = "Required";
  }

  if (values.password2 !== values.password) {
    errors.password2 = "Password must match!";
  }

  return errors;
};

interface IRenderer {
  input: WrappedFieldInputProps;
  label?: string;
  type: string;
  className?: string;
  meta: WrappedFieldMetaProps;
}

const renderField = ({
  input,
  label,
  type,
  className,
  meta: { touched, error, warning },
}: IRenderer): JSX.Element => (
  <div>
    <label>{label}</label>
    <div>
      <input {...input} className={className} placeholder={label} type={type} autoComplete={type} />
      {touched &&
        ((error && <span className="text-danger">{error}</span>) ||
          (warning && <span className="text-danger">{warning}</span>))}
    </div>
  </div>
);

function mapStateToProps(state: IReduxState): IStateProps {
  return {
    errorMessage: state.auth.error,
  };
}

function mapDispatchToProps(dispatch: Dispatch<ActionTypes>): IDispatchProps {
  return {
    signupUser: (userInfo: IUserInfo) => dispatch(signupUser(userInfo)),
  };
}

const form = connect<IStateProps, IDispatchProps, {}, IReduxState>(
  mapStateToProps,
  mapDispatchToProps,
)(Signup);
export default reduxForm<IFormProps>({ form: "signup", validate })(form);
