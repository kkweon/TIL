import * as React from "react";
import {connect } from "react-redux";
import {Redirect} from "react-router-dom";
import { IReduxState } from "../../reducers";

export default (ComposedComponent: React.ComponentClass<any> | React.StatelessComponent<any>): React.ComponentClass<any> | React.StatelessComponent<any> => {

  interface IStateToProps {
    authenticated: boolean;
  }

  class App extends React.Component<IStateToProps> {

    render() {
      if (this.props.authenticated)
        return <ComposedComponent {...this.props} />
      else
        return <Redirect to="/signin" />
    }
  }

  function mapStateToProps(state: IReduxState): IStateToProps {

    return {
      authenticated: state.auth.authenticated
    }
  }
  return connect<any, {}, {}, IReduxState>(mapStateToProps)(App)
}