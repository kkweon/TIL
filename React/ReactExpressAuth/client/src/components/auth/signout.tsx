import * as React from "react";
import { Redirect } from "react-router";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { signOutUser } from "../../actions";
import { ISignOut } from "../../actions/types";

interface IDispatchProps {
  signout: () => void;
}
type IProps = IDispatchProps;
const SignOut = (props: IProps) => {
  props.signout();
  return <Redirect to="/signin" />;
};

function mapDispatchToProps(dispatch: Dispatch<ISignOut>): IDispatchProps {
  return {
    signout: () => {
      dispatch(signOutUser());
    },
  };
}
export default connect(null, mapDispatchToProps)(SignOut);
