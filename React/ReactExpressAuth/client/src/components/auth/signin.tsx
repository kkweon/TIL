import * as React from "react";
import {Field, reduxForm, InjectedFormProps } from "redux-form";
import * as actions from "../../actions";
import { connect } from "react-redux";
import { IReduxState } from "../../reducers";

interface IFormData {
  email: string;
  password: string;
}

interface IFormProps {
  signinUser: ({ email, password }: IFormData) => void;
}

interface IStateProps {
  errorMessage: string | null;
}

type AllProps = IFormProps & InjectedFormProps<IFormData> & IStateProps;

class SignIn extends React.Component<AllProps> {
  private handleSubmit({ email, password }: IFormData) {
    this.props.signinUser({ email, password });
  }

  private renderAlert(): JSX.Element | undefined {
    if (this.props.errorMessage)
      return (
        <div className="alert alert-danger">
          {" "}
          <strong>Oops! </strong>
          {this.props.errorMessage}{" "}
        </div>
      );
    return;
  }

  public render() {
    const { handleSubmit } = this.props;
    return (
      <form onSubmit={handleSubmit(this.handleSubmit.bind(this))}>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <Field
            className="form-control"
            name="email"
            component="input"
            type="email"
            autoComplete="username"
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Password</label>
          <Field
            className="form-control"
            name="password"
            component="input"
            type="password"
            autoComplete="current-password"
          />
        </div>

        {this.renderAlert()}
        <button type="submit" className="btn btn-primary">
          Sign In
        </button>
      </form>
    );
  }
}

function mapStateToProps(state: IReduxState): IStateProps {
  return { errorMessage: state.auth.error };
}

const signinForm = connect<IStateProps, IFormProps, {}, IReduxState>(
  mapStateToProps,
  actions,
)(SignIn);

export default reduxForm<IFormData>({ form: "signin" })(signinForm);
