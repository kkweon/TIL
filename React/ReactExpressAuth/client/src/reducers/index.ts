import { combineReducers } from "redux";
import { reducer as form } from "redux-form";
import auth from "./auth_reducers";
import message from "./message_reducers";

const rootReducer = combineReducers({
  auth,
  form,
  message,
});

export interface IAuthState {
  authenticated: boolean;
  error: string | null;
}

export interface IReduxState {
  auth: IAuthState;
  form: any;
  message: string;
}

export default rootReducer;
