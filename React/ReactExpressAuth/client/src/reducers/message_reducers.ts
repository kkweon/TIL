import { ActionTypeKeys, ActionTypes } from "../actions/types";

export default (state: string = "", action: ActionTypes): string => {
  switch (action.type) {
    case ActionTypeKeys.RECEIVE_DATA:
      return action.payload;
    default:
      return state;
  }
};
