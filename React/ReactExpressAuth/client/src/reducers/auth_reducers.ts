import { IAuthState } from ".";
import { ActionTypeKeys, ActionTypes } from "../actions/types";

export default (
  state: IAuthState = { error: null, authenticated: false },
  action: ActionTypes,
): IAuthState => {
  switch (action.type) {
    case ActionTypeKeys.AUTH_USER:
      return { ...state, authenticated: true };
    case ActionTypeKeys.UNAUTH_USER:
      localStorage.removeItem("token");
      return { ...state, authenticated: false };
    case ActionTypeKeys.AUTH_ERROR:
      return { ...state, error: action.payload };
    case ActionTypeKeys.CLEAR_ERROR:
      return { ...state, error: null };
    default:
      return state;
  }
};
