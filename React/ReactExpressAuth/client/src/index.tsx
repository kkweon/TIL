//@flow
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import { Router, Route, Switch } from "react-router-dom";
import createHistory from "history/createBrowserHistory";
import reduxThunk from "redux-thunk";
import logger from "redux-logger";

import App from "./components/app";
import Feature from "./components/feature";
import Header from "./components/header";
import SignIn from "./components/auth/signin";
import SignUp from "./components/auth/signup";
import SignOut from "./components/auth/signout";
import requireAuth from "./components/auth/require_auth";
import reducers from "./reducers";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.css";
import { signin } from "./actions";

export const history = createHistory();
const createStoreWithMiddleware = applyMiddleware(logger, reduxThunk)(
  createStore,
);
const store = createStoreWithMiddleware(reducers)

const mountNode = document.getElementById("app");
if (!mountNode) throw new Error("mountNode is not found");

const token = localStorage.getItem("token");

if (token) {
  // update application state
  store.dispatch(signin(token))
}



ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <div>
        <Header />
        <div className="container pt-5">
          <Switch>
            <Route exact path="/" component={App} />
            <Route path="/feature" component={requireAuth(Feature)} />
            <Route path="/signin" component={SignIn} />
            <Route path="/signup" component={SignUp} />
            <Route path="/signout" component={SignOut} />
          </Switch>
        </div>
      </div>
    </Router>
  </Provider>,
  mountNode,
);
