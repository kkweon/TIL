import axios, { AxiosResponse } from "axios";
import { Dispatch } from "react-redux";
import { history } from "../../src";
import * as Types from "./types";
import { IReceiveData } from "./types";

const ROOT_URL = "http://0.0.0.0:4000";

interface ISigninResponse {
  token: string;
}

export interface IUserInfo {
  email: string;
  password: string;
}

export function signinUser({ email, password }: IUserInfo) {
  return (dispatch: Dispatch<Types.ActionTypes>): void => {
    axios
      .post(`${ROOT_URL}/auth/signin`, { email, password })
      .then((res: AxiosResponse<ISigninResponse>) => {
        dispatch(signin(res.data.token));

        localStorage.setItem("token", res.data.token);
        history.push("/feature");
      })
      .catch(() => {
        dispatch(authError("Bad Login Information"));
        setTimeout(() => dispatch(clearError()), 3000);
      });
  };
}

export function signupUser({ email, password }: IUserInfo) {
  return (dispatch: Dispatch<Types.ActionTypes>): void => {
    axios
      .post(`${ROOT_URL}/auth/signup`, { email, password })
      .then((res: AxiosResponse<ISigninResponse>) => {
        dispatch(signin(res.data.token));
        localStorage.setItem("token", res.data.token);
        history.push("/feature");
      })
      .catch(() => {
        dispatch(authError("Bad Information"));
        setTimeout(() => dispatch(clearError()), 3000);
      });
  };
}

export function signOutUser(): Types.ISignOut {
  return {
    type: Types.ActionTypeKeys.UNAUTH_USER,
  };
}

export function authError(error: string): Types.IAuthError {
  return {
    payload: error,
    type: Types.ActionTypeKeys.AUTH_ERROR,
  };
}

function clearError(): Types.IClearError {
  return {
    type: Types.ActionTypeKeys.CLEAR_ERROR,
  };
}

export function signin(token: string): Types.ISignIn {
  return {
    payload: token,
    type: Types.ActionTypeKeys.AUTH_USER,
  };
}

export function fetchMessage() {
  interface IMessage {
    message: string;
  }
  return (dispatch: Dispatch<IReceiveData>): void => {
    axios
      .get(`${ROOT_URL}/auth/`, {
        headers: {
          Authorization: localStorage.getItem("token"),
        },
      })
      .then((res: AxiosResponse<IMessage>) => {
        dispatch({
          payload: res.data.message,
          type: Types.ActionTypeKeys.RECEIVE_DATA,
        });
      })
      .catch(() => {});
  };
}
