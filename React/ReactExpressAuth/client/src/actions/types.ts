export enum ActionTypeKeys {
  AUTH_USER = "AUTH_USER",
  UNAUTH_USER = "UNAUTH_USER",
  AUTH_ERROR = "AUTH_ERROR",
  CLEAR_ERROR = "CLEAR_ERROR",

  RECEIVE_DATA = "RECEVE_DATA",
}
type Token = string;

export interface ISignIn {
  readonly type: ActionTypeKeys.AUTH_USER;
  readonly payload: Token;
}

export interface ISignOut {
  readonly type: ActionTypeKeys.UNAUTH_USER;
}

export interface IAuthError {
  readonly type: ActionTypeKeys.AUTH_ERROR;
  readonly payload: string;
}

export interface IClearError {
  readonly type: ActionTypeKeys.CLEAR_ERROR;
}

export interface IReceiveData {
  readonly type: ActionTypeKeys.RECEIVE_DATA;
  readonly payload: string;
}

export type ActionTypes = ISignIn | ISignOut | IAuthError | IClearError | IReceiveData;
