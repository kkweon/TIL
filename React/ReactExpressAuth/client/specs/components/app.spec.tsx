//@flow
import * as React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import App from "../../src/components/app";
import { expect } from "chai";

describe("App", () => {
  let component: ShallowWrapper<any, any>;

  beforeEach(() => {
    component = shallow(<App />);
  });

  it("renders something", () => {
    expect(component).to.exist;
  });

  it("clicks nothing happends", () => {
    component.simulate("click")
    expect(component).to.exist
  })
});
