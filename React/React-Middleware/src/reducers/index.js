//@flow
import { combineReducers } from "redux";
import usersReducer from "./users";

export type ReduxState = {
  users: typeof usersReducer,
};

const reduxState: ReduxState = {
  users: usersReducer,
};
const rootReducer = combineReducers(reduxState);

export default rootReducer;
