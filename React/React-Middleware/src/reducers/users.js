//@flow
import * as types from "../actions/types";

type Action = types.FetchActionType | types.FetchDoneType;
export default function(state: types.User[] = [], action: Action): any {
  switch (action.type) {
    case types.FETCH_USERS:
      // it is not a promise
      if (Array.isArray(action.payload.data))
        return [...state, ...action.payload.data];
      return state;

    default:
      return state;
  }
}
