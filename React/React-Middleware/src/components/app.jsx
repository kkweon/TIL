//@flow
import React, { Component } from "react";
import UserList from "./user_list";

export default class App extends Component<any, void> {
  render() {
    return (
      <div style={{ paddingTop: "2rem" }}>
        <UserList />
      </div>
    );
  }
}
