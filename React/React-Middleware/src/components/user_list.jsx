// @flow
import React, { Component } from "react";
import * as types from "../actions/types";
import type { ReduxState } from "../reducers";
import { connect } from "react-redux";
import * as actions from "../actions";

type Props = {
  users: types.User[],
  fetchUsers: () => any,
};

type State = {};

class UserList extends Component<Props, State> {
  componentWillMount() {
    this.props.fetchUsers();
  }

  renderUser(user: types.User) {
    return (
      <div key={user.name} className="card card-block">
        <h4 className="card-title">{user.name}</h4>
        <p className="card-text">
          {(user.company && user.company.name) || "Cheese Cake Factory"}
        </p>
        <a className="btn btn-primary" href={`http://${user.website || ""}`}>
          Website
        </a>
      </div>
    );
  }

  render() {
    return (
      <div className="user-list">{this.props.users.map(this.renderUser)}</div>
    );
  }
}

function mapStateToProps(state: ReduxState): { users: any } {
  return {
    users: state.users,
  };
}

export default connect(mapStateToProps, actions)(UserList);
