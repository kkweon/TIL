//@flow
export const FETCH_USERS = "FETCH_USERS";

export type User = {
  id?: number,
  name: string,
  username?: string,
  email?: string,
  address?: {
    street?: string,
    suite?: string,
    city?: string,
    zipcode?: string,
    geo?: {
      lat?: string,
      lng?: string,
    },
  },
  phone?: string,
  website?: string,
  company?: {
    name?: string,
    catchPhrase?: string,
    bs?: string,
  },
};

export type FetchActionType = {
  type: typeof FETCH_USERS,
  payload: Promise<any>,
};

export type FetchDoneType = {
  type: typeof FETCH_USERS,
  payload: { data: User[] },
};
