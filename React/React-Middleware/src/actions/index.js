//@flow
import * as axios from "axios";
import * as types from "./types";

const URL = "https://jsonplaceholder.typicode.com/users";
export function fetchUsers(): types.FetchActionType {
  const request = axios.get(URL);
  return {
    type: types.FETCH_USERS,
    payload: request,
  };
}
