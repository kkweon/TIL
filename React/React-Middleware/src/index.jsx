//@flow
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

import App from "./components/app";
import reducers from "./reducers";
import Async from "./middleware/async";

const createStoreWithMiddleware = applyMiddleware(Async)(createStore);
const mountNode = document.querySelector(".container");
if (!mountNode) throw new Error("mountNode is not found");

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <App />
  </Provider>,
  mountNode,
);
