// @flow
export default ({ dispatch }: any) => (next: any) => (action: any): any => {
  if (!action.payload || !action.payload.then) return next(action);

  action.payload
    .then(response =>
      dispatch({
        ...action,
        payload: response,
      }),
    )
    .catch(err => {
      console.error("[FAIL] Fetching data");
    });
};
